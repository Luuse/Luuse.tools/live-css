# Live Css

Live Css is a small tool based on Grav Cms and Etherpad, that allows users to create a collaborative web-to-print poster. It was made by Brussels based [Luuse](http://luuse.io) for a workshop taking place at the 2019 [Fig Festival](http://www.figliege.com/) (Liège, BE). It's aimed to be modified, extended, improved... Have fun!

## Requirements

- Apache server
- [Wget](https://www.gnu.org/software/wget/) (for install)


## Install (Unix systems)

1. Go to your download folder (under server root), then run

``` git
$ git clone https://gitlab.com/Luuse/Luuse.tools/live-css.git
``` 

2. To install the [Grav](https://getgrav.org/) based website and an [Etherpad](https://etherpad.org/) instance locally, run

``` 
$ make 
```

3. With a browser, go to your **live-css** local url (http://localhost/chosen-place-for-live-css/site)

4. You're asked to create a Grav Account. Do so.

5. Open a terminal window. Go to your etherpad folder, then run the following command to start etherpad.

``` 
$ bin/run.sh
```

6. Go to your local etherpad url, most likely [http://0.0.0.0:9001](http://0.0.0.0:9001)

7. Create a new pad, and call it *live-css*

8. Go back to the web page (http://localhost/chosen-place-for-live-css/site). Everything should be working fine!

## Change pad

You're free to use any pad url you want, having etherpad installed locally is not necessary. If you want to change the pad url:

1. Go to the admin part of Grav, then > Plugin > Padtocss. 

2. Change the *Pad Url* field to your chosen url. Don't forget *http* or it'll crash. 