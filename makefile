pathRepo = $(shell pwd)

install:
	wget https://getgrav.org/download/core/grav-admin/1.4.6/grav-admin-v1.4.6.zip
	unzip grav*.zip
	rm grav*.zip
	sudo chmod -R 777 grav-admin/
	mv grav-admin/ site/
	mv $(pathRepo)/padtocss/ $(pathRepo)/site/user/plugins/padtocss/ 
	rm -r $(pathRepo)/site/user/pages/
	mv $(pathRepo)/pages/ $(pathRepo)/site/user/pages/
	rm -r $(pathRepo)/site/user/themes/quark
	mv $(pathRepo)/live-css/ $(pathRepo)/site/user/themes
	rm $(pathRepo)/site/user/config/system.yaml
	mv $(pathRepo)/system.yaml $(pathRepo)/site/user/config/system.yaml
	mkdir $(pathRepo)/site/user/config/plugins
	mv $(pathRepo)/padtocss.yaml $(pathRepo)/site/user/config/plugins/padtocss.yaml	
	git clone --branch master git://github.com/ether/etherpad-lite.git

