function ajax(receiver, target){

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (this.readyState == 4 && this.status == 200) {

      var str = '<style type="text/css" id="style">';
      var begin = this.responseText.indexOf(str);
      var l = str.length;
      var end = this.responseText.indexOf("</style>");
      var length = end-(begin+l);

      receiver.innerText= this.responseText.substr(begin+l, length);

    }
  };
  xhttp.open("GET", target);
  xhttp.send();

}

function refresh(){

  var receiver = document.getElementById("style");
  var button = document.getElementById("refresh");
  var target = window.location.href;

  button.addEventListener("click", function(){

    ajax(receiver, target);

  });
}

function setBackUp(){

  var backUp = document.getElementById("#backup");

  backup.addEventListener("click", function(){

    var form = document.getElementById("formCommit");

    if(form.classList.contains("hidden")){

      form.classList.remove("hidden");

    }else{

      form.classList.add("hidden");
    }

  });


}

function setPrint(){

  var print = document.getElementById("print");

  print.addEventListener("click", function(){

    window.print();

  });
}


  setPrint();
  refresh();
  setBackUp();


