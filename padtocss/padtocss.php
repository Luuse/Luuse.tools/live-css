<?php

namespace Grav\Plugin;

use \Grav\Common\Plugin;
use \Grav\Common\Page\Pages;
use \Grav\Common\Page\Page;
use \Grav\Common\Page\Collection;

class PadtocssPlugin extends Plugin{

    public static function getSubscribedEvents(){

        return [

            'onPluginsInitialized' => ['onPluginsInitialized', 0],
            'onTwigExtensions' => ['onTwigExtensions', 0],
            'onPagesInitialized' => ['onPagesInitialized', 0]
        ];
    }

    public function getFolderName($route){

        $slash = strpos($route, "/") + 1;

        return substr($route, $slash);

    }

    public function getCss(){

        $padUrl = $this->config->get('plugins.padtocss.padUrl')."/export/txt";

        return file_get_contents($padUrl);

    }

    public function posterRoute(){

        return $this->getFolderName($this->config->get('plugins.padtocss.posterRoute'));
    }

    public function isPosterPage(){

        $uri = $this->grav['uri'];
        $homeRoute = $this->grav["pages"]->getHomeRoute();
        $posterRoute = $this->posterRoute();

        $checkPath = ($homeRoute == $posterRoute) ? "/" : $posterRoute;

        return ($uri->path() == $checkPath);

    }

    public function fileRoute(){

        $posterRoute = $this->posterRoute();
        return  "user/pages/".$posterRoute."/pad.css";
    }

    public function onTwigExtensions(){

        if ($this->isPosterPage()){

            $file = $this->fileRoute();
            $css = file_get_contents($file);

            require_once(__DIR__ . '/twig/PadTwigExtension.php');
            $this->grav['twig']->twig->addExtension(new PadTwigExtension($css));

        }

    }

    public function onPluginsInitialized(){

        if ($this->isPosterPage()){

            $file = $this->fileRoute();
            $css = $this->getCss();


            file_put_contents($file, $css); 

        }

    }

    public function onPagesInitialized(){

        $uri = $this->grav['uri'];
        $historyRoute = $this->config->get('plugins.padtocss.historyRoute');
        $historyMode = $this->config->get('plugins.padtocss.historyMode');


        if($uri->path() == $historyRoute && $historyMode && !empty($_POST)){

            $posterRoute = $this->config->get('plugins.padtocss.posterRoute');
            $posterPage = $this->grav['pages']->find($posterRoute);
            $historyPage = $this->grav['pages']->find($historyRoute);
            $folderName = "poster_".time();

            $copy = $posterPage->copy($historyPage);
            $copy->folder($folderName);
            $copy->save();

            $commitDatas = $_POST;
            $mdFile = "user/pages".$historyRoute."/".$folderName."/poster.md";
            $content = file_get_contents($mdFile);
            $ok = str_replace("---", "", $content);
            $array = [];

            foreach($commitDatas as $name=>$data){

                $array [] = $name.": ".$data;

            }

            $string = "---\n".trim($ok)."\n".implode("\n", $array)."\n---";
            file_put_contents($mdFile, $string);
            $this->grav["grav"]->redirect($historyRoute);


        }

    }
}