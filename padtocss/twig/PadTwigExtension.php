<?php
namespace Grav\Plugin;

use \Grav\Common\Plugin;


class PadTwigExtension extends \Twig_Extension{

    public function __construct($first){

        $this->css = $first;
    }

    public function getName(){
        return 'PadTwigExtension';
    }
    
    public function getFunctions(){
        return [
            new \Twig_SimpleFunction('loadPad', [$this, 'padToCss'])
        ];
    }

    public function padToCss(){

        return '<style type="text/css" id="style">'.$this->css."</style>"; 
    }
}